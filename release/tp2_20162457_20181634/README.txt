# TP2 Louis Lalonde (20162457) & Alexandre Hamila (20181634)

## Developement

Repo: https://gitlab.com/udem3/ift3913/tp2

### Prerequisities

1. openjdk >= 11.0.16
2. Apache Maven >= 3.6.3

## Comment déployer l'application (deploy)

1. cd à la source du dossier du projet
2. `sudo chmod +x release.sh`
3. `bash release.sh`

## Tutoriel

### Comment lancer l'application:

1. Créer un dossier csv s'il n'existe pas à la source de l'application
2. `java -jar tp2-analyze.jar <ARG_1> <ARG_2>`  
   <ARG_1> : Chemin d'accès vers le projet à analyser [String]  
   <ARG_2> : Exécution de la métrique CM (~ 3 minutes) [Boolean]

Si <ARG_2> est définit à false, alors l'application réutilise
le dernier fichier sortie produit par le pipeline de class maturity.
Si le fichier n'est pas accessible, l'analyse ne peut pas être effectuée
et l'utilisateur est invité à exécuter l'analyze dans son intégralitée.

**Note importante**: L'éxécution de la métrique CM se fait à l'aide de l'API REST de Github qui nécessite
une clé (path: src/main/config/maturity.properties). Github applique du rate-limiting sur les requêtes vers ses APIs.
Il se peut donc que vous tombiez sur une erreur si vous executez le projet trop de fois avec <ARG_2> set to true.

Les fichiers csv se trouveront dans le dossier csv

### Exemples d'exécutions:

(On utilise le path $HOME/jfreechart/src/main/java si l'on ne veut pas prendre en considération les fichiers tests mais sinon juste utiliser le path $HOME/jfreechart/src)
**Dans notre analyse du rapport nous n'avons pas pris en considération les fichiers de test **

java -jar tp2-analyze.jar $HOME/jfreechart/src/main/java false  
java -jar tp2-analyze.jar $HOME/jfreechart/src/main/java true

ou bien:

java -jar tp2-analyze.jar $HOME/jfreechart/src false  
java -jar tp2-analyze.jar $HOME/jfreechart/src true

## Ressources

### Échelle intervalle pour la complexité cyclomatique de McCabe

[Échelle](https://www.guru99.com/cyclomatic-complexity.html)

### Échelle personnelle pour la métrique de maturité des classes

| score | description             |
| ----- | ----------------------- |
| 0-2   | non mature              |
| 3-10  | peu mature              |
| 11-30 | moyennement mature      |
| 31-40 | considérablement mature |
| 41-\* | très mature             |

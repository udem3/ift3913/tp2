package com.udem;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import com.opencsv.exceptions.CsvException;
import com.udem.applications.Dcr;
import com.udem.applications.Maturity;
import com.udem.entities.Csv;
import com.udem.entities.Metric;
import com.udem.entities.ProgramFile;
import com.udem.services.CsvUtils;
import com.udem.services.Displayer;
import com.udem.services.InputParser;

// General pipeline for the maintenability analysis of a project
public class Analyze implements CsvUtils {
    private static final String[] header = { "chemin du fichier", "nom du paquet",
            "nom de la classe", "cloc metric", "mc metric", "wmc metric", "dcr metric", "nch metric",
            "age metric", "cm metric" };

    public static void main(String[] args) throws Exception {
        Csv cmCsv = null;
        Csv analyzeCsv = new Csv();
        InputParser.analyze(args);
        if (Boolean.parseBoolean(args[1])) {
            cmCsv = evaluateCM(args);
        } else {
            System.out.println("Omitting the class maturity analysis... Reading from previous file");
            try {
                cmCsv = Metric.parse("cm_output.csv", null);
            } catch (Exception e) {
                throw new Error("cm_output.csv not in path... Please run the entire pipeline.\n" +
                        "Arg[1]: Run class maturity analysis (could take several minutes)[boolean], should be set to true.");
            }
        }
        Csv dcrCsv = evaluateDcr(args);
        evaluateDcrCmMetricCompatibility(dcrCsv, cmCsv);
        interpretMaturity(cmCsv.files);
        setMetric(cmCsv.files, dcrCsv.files, analyzeCsv);
        ArrayList<String[]> outputfiles = new Analyze().produceOuputContent(analyzeCsv.files);
        CsvUtils.write(outputfiles, "analyze.csv", header);
        System.out.println("Maintainability pipeline analysis is done!");
    }

    private static void evaluateDcrCmMetricCompatibility(Csv dcrCsv, Csv cmCsv){
        if (dcrCsv.files.size() != cmCsv.files.size()) {
            throw new Error("Dcr metric file doesn't contains the same exact number of class as the\n" +
            "CM metric file. Please run the entire pipeline.");
        }
    }

    private static Csv evaluateDcr(String[] args) throws Exception {
        Dcr.main(args);
        Csv dcrCsv = Metric.parse("dcr_output.csv", null);
        int qualifiedClass = Dcr.calculateMaintenableClassNumber(dcrCsv.files);
        double threshold = Metric.calculateThreshold(qualifiedClass, dcrCsv.files.size());
        boolean thresholdEvaluation = Metric.evaluateThreshold(threshold);
        interpretMeanCyclomaticComplexity(qualifiedClass, dcrCsv.files.size(), threshold, thresholdEvaluation);
        return dcrCsv;
    }

    private static Csv evaluateCM(String[] args) throws IOException, InterruptedException, CsvException {
        Displayer.separator();
        Maturity.main(args);
        Csv cmCsv = Metric.parse("cm_output.csv", null);

        return cmCsv;
    }

    private static void interpretMaturity(ArrayList<ProgramFile> cmCsv) {
        int[] maturityCount = Maturity.calculateMaturityClasses(cmCsv);
        Displayer.separator();
        System.out.println("Maturity assessment based on this table:");
        System.out.print("[- Score -    Result -]\n" +
                "[- 0-2  - not mature (Count: " + maturityCount[0] + ") -]\n" +
                "[- 3-10 - weak maturity (Count: " + maturityCount[1] + ") -]\n" +
                "[- 11-30 - medium maturity (Count: " + maturityCount[2] + ") -]\n" +
                "[- 31-40 - considerably mature (Count: " + maturityCount[3] + ") -]\n" +
                "[- 41+   - very mature (Count: " + maturityCount[4] + ") -]\n");
        Displayer.separator();

        int sumMature = maturityCount[1] + maturityCount[2] + maturityCount[3] + maturityCount[4];

        double score = ((double) sumMature / (sumMature + maturityCount[0]) * 100);

        System.out.println(String.format("%.2f", score) + "% of the programs' classes are mature");

        if (score >= 90) {
            System.out.println("Program is globally considered as well mature");
        } else {
            System.out.println("Program is not globally considered as mature");
        }
    }

    // Base on: https://www.guru99.com/cyclomatic-complexity.html chart
    private static void interpretMeanCyclomaticComplexity(int qualifiedClass, int numberClass, double threshold,
            boolean thresholdEvaluation) {
        Displayer.separator();
        System.out.println("Mean cyclomatic complexity assessment (mcc) scale is base on this chart:");
        System.out.println("https://www.guru99.com/cyclomatic-complexity.html");
        System.out.println("Program is considered well maintainable if 90%\nof the programs' " +
                "classes have a mcc value between 1 and 10");
        Displayer.separator();
        System.out.println("Program contains: " + numberClass + " class");
        System.out.println("Class considered as maintainable " + qualifiedClass);
        System.out.println(String.format("%.2f", (threshold * 100)) + "% of the programs' classes are maintainable");
        if (thresholdEvaluation) {
            System.out.println("Program is globally considered as well maintainable");
        } else {
            System.out.println("Program is not globally considered as well maintainable");
        }
    }

    private static void setMetric(ArrayList<ProgramFile> cmFiles, ArrayList<ProgramFile> dcrFiles, Csv analyzeCsv)
            throws FileNotFoundException {

        for (int i = 0; i < cmFiles.size(); i++) {
            if (cmFiles.get(i).getPathToFile().equals(dcrFiles.get(i).getPathToFile())) {
                if (cmFiles != null) {
                    analyzeCsv.addProgramFile(
                            new ProgramFile(cmFiles.get(i).getPathToFile(),
                                    cmFiles.get(i).getPaquetName(), cmFiles.get(i).getClassName(),
                                    new ArrayList<>(Arrays.asList(
                                            new Metric(dcrFiles.get(i).getMetrics().get(0).getValue(), "cloc"),
                                            new Metric(dcrFiles.get(i).getMetrics().get(1).getValue(), "mc"),
                                            new Metric(dcrFiles.get(i).getMetrics().get(2).getValue(), "wmc"),
                                            new Metric(dcrFiles.get(i).getMetrics().get(3).getValue(), "dcr"),
                                            new Metric(cmFiles.get(i).getMetrics().get(0).getValue(), "nch"),
                                            new Metric(cmFiles.get(i).getMetrics().get(1).getValue(), "age"),
                                            new Metric(cmFiles.get(i).getMetrics().get(2).getValue(), "cm")))));
                }
            }
        }
    }

    @Override
    public ArrayList<String[]> produceOuputContent(ArrayList<ProgramFile> file) {
        ArrayList<String[]> files = new ArrayList<>();
        for (ProgramFile line : file) {
            ArrayList<String> data = new ArrayList<>(Arrays.asList(line.getPathToFile(),
                    line.getPaquetName(), line.getClassName()));
            for (Metric metric : line.getMetrics()) {
                data.add(metric.getValue());
            }
            files.add(data.toArray(new String[0]));
        }
        return files;
    }
}

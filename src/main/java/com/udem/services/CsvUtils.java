package com.udem.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;
import com.udem.entities.ProgramFile;

public interface CsvUtils {
    public static final String outputDirectoryName = "./csv";
    public static final String relativeOutputPath = String.format("%s/", outputDirectoryName);

    public static void evaluateOutputDirectory(String relativeOutputPath) {
        Boolean inPath = new File(relativeOutputPath).exists();
        if (!inPath) {
            throw new Error(outputDirectoryName + " directory not found in path.\n" +
                    "Please create a directory with the name " + outputDirectoryName + " at the root of the\n"
                    +
                    "application.");
        }
    }

    public static List<String[]> read(String file) throws FileNotFoundException, IOException, CsvException {
        String filePath = String.format("%s%s", relativeOutputPath, file);
        CSVParser parser = new CSVParserBuilder().withSeparator(',').build();
        try (CSVReader reader = new CSVReaderBuilder(new FileReader(filePath)).withSkipLines(1).withCSVParser(parser)
                .build();) {
            return reader.readAll();
        }
    }

    public static void write(ArrayList<String[]> csvContent, String fileName, String[] header) {
        String outputPath = String.format("%s%s", relativeOutputPath, fileName);
        try {
            evaluateOutputDirectory(relativeOutputPath);
            CSVWriter writer = new CSVWriter(new FileWriter(outputPath));
            // Initializing the CSV
            writer.writeNext(header);
            for (String[] file : csvContent) {
                writer.writeNext(file);
            }
            writer.close();
            if (!fileName.equals("jls_output.csv")) {
                System.out.println(String.format("Ouput file can be found at : %s", outputPath));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void generateFileRelativePath(ArrayList<ProgramFile> csvContent, String directoryPath) {
        for (ProgramFile file : csvContent) {
            String path = directoryPath + file.getPathToFile().substring(1);
            file.setRelativePath(path);
        }
    }

    public ArrayList<String[]> produceOuputContent(ArrayList<ProgramFile> csvLines) throws IOException, InterruptedException;

}

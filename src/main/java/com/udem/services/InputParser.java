package com.udem.services;

import java.util.HashMap;

public class InputParser {


    public static HashMap<String, Object> lcsec(String args[]) throws Exception {
        // Static data
        int inputArgsNumber = args.length;
        int argNumber = 1;
        HashMap<String, Object> parsedArgs = new HashMap<>();

        if (inputArgsNumber < argNumber) {
            throw new Exception(String.format("Missing %d", argNumber - inputArgsNumber, "arguments on the command line"));
        } else if (inputArgsNumber > argNumber + 1) {
            throw new Exception("To much arguments passed on the command line");
        } else {
            parsedArgs.put("directoryPath", args[0]);
        }
        return parsedArgs;
    }

    public static HashMap<String, Object> egon(String args[]) throws Exception {
        // Static data
        int inputArgsNumber = args.length;
        int argNumber = 2;
        HashMap<String, Object> parsedArgs = new HashMap<>();

        if (inputArgsNumber < argNumber) {
            throw new Exception(String.format("Missing %d", argNumber - inputArgsNumber, " arguments on the command line"));
        } else if (inputArgsNumber > argNumber) {
            throw new Exception("To much arguments passed on the command line");
        } else {
            parsedArgs.put("directoryPath", args[0]);
            parsedArgs.put("seuil", Integer.parseInt(args[1]));
        }
        return parsedArgs;
    }

    public static String[] analyze(String[] args) {
        if (args.length == 1) {
            throw new Error("Supply values for the following parameters:\n" +
            "Project folder path[String]:\n" +
            "Run class maturity analysis (could take several minutes)[boolean]:");
        } else if (args.length > 3) {
            throw new Error("Too much arguments. Supply values for the following parameters:\n" +
            "Project folder path[String]:\n" +
            "Run class maturity analysis (could take several minutes)[boolean]:");
        } else {
            return args;
        }
    }


}

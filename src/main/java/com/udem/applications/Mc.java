package com.udem.applications;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import com.udem.entities.Csv;
import com.udem.entities.Metric;
import com.udem.services.CsvUtils;
import com.udem.services.InputParser;
import com.udem.entities.ProgramFile;

// Evaluate the number of method per class
public class Mc implements CsvUtils {
    private static final String[] header = { "chemin du fichier", "nom du paquet",
            "nom de la classe", "mc metric", };

    public static void main(String[] args) throws Exception {
        Jls.main(args);
        HashMap<String, Object> parsedArgs = InputParser.lcsec(args);
        // // Reading files produced by the jls app
        List<String[]> jlsFiles = CsvUtils.read("jls_output.csv");
        Csv csv = new Csv();
        csv.parse(jlsFiles, "jls");
        ArrayList<ProgramFile> files = csv.files;
        CsvUtils.generateFileRelativePath(files, parsedArgs.get("directoryPath").toString());
        setMetric(files);
        ArrayList<String[]> outputfiles = new Mc().produceOuputContent(files);
        CsvUtils.write(outputfiles, "mc_output.csv", header);
        System.out.println("mc is done!");
    }

    public static void setMetric(ArrayList<ProgramFile> files) throws FileNotFoundException {
        Mc mc = new Mc();
        for (ProgramFile file : files) {
            Integer mcMetric = mc.evaluate(file.getRelativePath());
            file.setMetrics(
                    new ArrayList<>(Arrays.asList(
                            new Metric(Integer.toString(mcMetric), "cm"))));
        }
    }

    public Integer evaluate(String classPath) throws FileNotFoundException {
        FileInputStream fis = new FileInputStream(classPath);
        Scanner sc = new Scanner(fis);
        int methodCountMetric = 0;

        while (sc.hasNextLine()) {
            String nextLine = sc.nextLine();
            if (!nextLine.contains("//") && nextLine.length() > 0) {
                if (nextLine.contains("public")){
                    methodCountMetric++;
                } else if (nextLine.contains("protected")){
                    methodCountMetric++;
                } else if (nextLine.contains("private")){
                    methodCountMetric++;
                }
            }
        }

        return methodCountMetric;
    }

    @Override
    public ArrayList<String[]> produceOuputContent(ArrayList<ProgramFile> csvLines) {
        ArrayList<String[]> files = new ArrayList<>();
        for (ProgramFile line : csvLines) {
            ArrayList<String> data = new ArrayList<>(Arrays.asList(line.getPathToFile(),
                    line.getPaquetName(), line.getClassName()));
            for (Metric metric : line.getMetrics()) {
                data.add(metric.getValue());
            }
            files.add(data.toArray(new String[0]));
        }
        return files;
    }
}

package com.udem.applications;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;

import com.udem.entities.ProgramFile;
import com.udem.entities.Csv;
import com.udem.entities.Number;
import com.udem.entities.Metric;
import com.udem.services.CsvUtils;

// Evaluates the ratio between the number of comments of a class and it's
// weigthed method complexity
public class Dcr implements CsvUtils {
    private static final String[] header = { "chemin du fichier", "nom du paquet",
            "nom de la classe", "cloc metric", "mc metric", "wmc metric", "dcr metric" };

    public static void main(String args[]) throws Exception {
        Csv dcrCsv = new Csv();
        Csv clocCsv = parseClocMetric(args);
        Csv mcMetric = parseMcMetric(args);
        Csv wmcCsv = parseWmcMetric(args);
        setMetric(clocCsv.files, mcMetric.files, wmcCsv.files, dcrCsv);
        ArrayList<String[]> outputfiles = new Dcr().produceOuputContent(dcrCsv.files);
        CsvUtils.write(outputfiles, "dcr_output.csv", header);
        System.out.println("Dcr is done!");
    }

    private static void evaluateMetricDependencies(ArrayList<ProgramFile> clocFiles,
            ArrayList<ProgramFile> wmcFiles) {
        if (clocFiles.size() != wmcFiles.size()) {
            throw new Error("Cloc Wmc metric files aren't of the same size... Stoping the metric evaluation.");
        }
    }

    private static void setMetric(ArrayList<ProgramFile> clocFiles, ArrayList<ProgramFile> mcFiles,
            ArrayList<ProgramFile> wmcFiles, Csv dcrCsv) throws FileNotFoundException {
        evaluateMetricDependencies(clocFiles, wmcFiles);

        for (int i = 0; i < clocFiles.size(); i++) {
            if (clocFiles.get(i).getPathToFile().equals(wmcFiles.get(i).getPathToFile())) {
                Integer clocMetric = Integer.parseInt(clocFiles.get(i).getMetrics().get(0).getValue());
                Integer mcMetric = Integer.parseInt(mcFiles.get(i).getMetrics().get(0).getValue());
                Integer wmcMetric = Integer.parseInt(wmcFiles.get(i).getMetrics().get(0).getValue());
                Double dcrMetric = evaluate(clocMetric, wmcMetric);

                dcrCsv.addProgramFile(
                        new ProgramFile(clocFiles.get(i).getPathToFile(),
                                clocFiles.get(i).getPaquetName(), clocFiles.get(i).getClassName(),
                                new ArrayList<>(Arrays.asList(
                                        new Metric(Integer.toString(clocMetric), "cloc"),
                                        new Metric(Integer.toString(mcMetric), "mc"),
                                        new Metric(Integer.toString(wmcMetric), "wmc"),
                                        new Metric(Double.toString(dcrMetric), "dcr")))));
            }
        }
    }

    private static Csv parseClocMetric(String args[]) throws Exception {
        Cloc.main(args);
        return Metric.parse("cloc_output.csv", "cloc");
    }

    private static Csv parseMcMetric(String args[]) throws Exception {
        Mc.main(args);
        return Metric.parse("mc_output.csv", "mc");
    }

    private static Csv parseWmcMetric(String args[]) throws Exception {
        Wmc.main(args);
        return Metric.parse("wmc_output.csv", "wmc");
    }

    private static double evaluateDocumentComplexityRatio(Integer cloc, Integer wmc) {
        return Number.round( cloc / wmc);
    }

    public static Double evaluate(Integer clocMetric, Integer wmcMetric) throws FileNotFoundException {
        return evaluateDocumentComplexityRatio(clocMetric, wmcMetric);
    }

    public static int calculateMaintenableClassNumber(ArrayList<ProgramFile> dcrFiles) {
        int qualifiedClass = 0;
        for (ProgramFile file : dcrFiles) {
            int classMc = Integer.parseInt(file.getMetrics().get(1).getValue());
            int classWmc = Integer.parseInt(file.getMetrics().get(2).getValue());
            double classMeanMcc = (double) classWmc / classMc;
            if (classMeanMcc <= 10) {
                qualifiedClass++;
            }
        }
        return qualifiedClass;
    }

    @Override
    public ArrayList<String[]> produceOuputContent(ArrayList<ProgramFile> file) {
        ArrayList<String[]> files = new ArrayList<>();
        for (ProgramFile line : file) {
            ArrayList<String> data = new ArrayList<>(Arrays.asList(line.getPathToFile(),
                    line.getPaquetName(), line.getClassName()));
            for (Metric metric : line.getMetrics()) {
                data.add(metric.getValue());
            }
            files.add(data.toArray(new String[0]));
        }
        return files;
    }

}

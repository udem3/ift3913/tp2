package com.udem.applications;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.udem.entities.Csv;
import com.udem.entities.ProgramFile;
import com.udem.services.CsvUtils;

// See TP1 for method protocol
public class Jls implements CsvUtils {
    // Static Data
    private static final String[] header = { "chemin du fichier", "nom du paquet", "nom de la classe" };

    public static void main(String args[]) throws IOException {
        Csv csv = new Csv();
        getJavaFiles(args[0], "", csv);
        ArrayList<String[]> files = new Jls().produceOuputContent(csv.files);
        CsvUtils.write(files, "jls_output.csv", header);
    }

    // https://www.baeldung.com/java-remove-last-character-of-string
    public static String removeLastChar(String s) {
        return (s == null || s.length() == 0)
                ? null
                : (s.substring(0, s.length() - 1));
    }

    private static void getJavaFiles(String path, String dir, Csv csv) {
        File[] wholeDir = new File(path).listFiles();
        if (wholeDir == null) {
            return;
        } else {
            for (File file : wholeDir) {
                if (file.isFile() && file.getName().endsWith(".java")) {
                    String paquetName = removeLastChar(dir);
                    String pathToFile = "./" + dir.replace(".", "/") + file.getName();
                    csv.addProgramFile(new ProgramFile(pathToFile, paquetName, file.getName(), null));
                } else if (file.isDirectory()) {
                    getJavaFiles(file.getAbsolutePath(), dir + file.getName() + ".", csv);
                }
            }
        }
    }

    @Override
    public ArrayList<String[]> produceOuputContent(ArrayList<ProgramFile> csvLines) {
        ArrayList<String[]> files = new ArrayList<>();
        for (ProgramFile line : csvLines) {
            files.add(new String[] { line.getPathToFile(), line.getPaquetName(), line.getClassName() });
        }
        return files;
    }
}
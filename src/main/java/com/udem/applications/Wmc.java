package com.udem.applications;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import com.udem.entities.Csv;
import com.udem.entities.Metric;
import com.udem.entities.ProgramFile;
import com.udem.services.CsvUtils;
import com.udem.services.InputParser;

// Evaluate the weigthed method complexity of a class
public class Wmc implements CsvUtils {
    // Static Data
    private static final String[] header = { "chemin du fichier", "nom du paquet",
            "nom de la classe", "wmc metric" };

    public static void main(String args[]) throws Exception {
        Jls.main(args);
        HashMap<String, Object> parsedArgs = InputParser.lcsec(args);
        // // Reading files produced by the jls app
        List<String[]> jlsFiles = CsvUtils.read("jls_output.csv");
        Csv csv = new Csv();
        csv.parse(jlsFiles, "jls");
        ArrayList<ProgramFile> files = csv.files;
        CsvUtils.generateFileRelativePath(files, parsedArgs.get("directoryPath").toString());
        setMetric(files);
        ArrayList<String[]> outputfiles = new Wmc().produceOuputContent(files);
        CsvUtils.write(outputfiles, "wmc_output.csv", header);
        System.out.println("Wmc is done!");

    }

    private static void setMetric(ArrayList<ProgramFile> files) throws FileNotFoundException {
        Wmc wmc = new Wmc();
        for (ProgramFile file : files) {
            Integer wmcMetric = wmc.evaluate(file.getRelativePath());
            file.setMetrics(
                    new ArrayList<>(Arrays.asList(
                            new Metric(Integer.toString(wmcMetric), "wmc"))));
        }
    }

    public Integer evaluate(String classPath) throws FileNotFoundException {
        FileInputStream fis = new FileInputStream(classPath);
        Scanner sc = new Scanner(fis);
        int wmcMetric = 1;

        while (sc.hasNextLine()) {
            String nextLine = sc.nextLine();
            // Line is not considered as a comment and isn't empty
            if (!nextLine.contains("//") && nextLine.length() > 0) {
                if (nextLine.contains("if")) {
                    wmcMetric++;
                } else if (nextLine.contains("for")) {
                    wmcMetric++;
                } else if (nextLine.contains("while")) {
                    wmcMetric++;
                } else if (nextLine.contains("switch")) {
                    wmcMetric++;
                }
            }
        }
        return wmcMetric;
    }

    @Override
    public ArrayList<String[]> produceOuputContent(ArrayList<ProgramFile> csvLines) {
        ArrayList<String[]> files = new ArrayList<>();
        for (ProgramFile line : csvLines) {
            ArrayList<String> data = new ArrayList<>(Arrays.asList(line.getPathToFile(),
                    line.getPaquetName(), line.getClassName()));
            for (Metric metric : line.getMetrics()) {
                data.add(metric.getValue());
            }
            files.add(data.toArray(new String[0]));
        }
        return files;
    }
}

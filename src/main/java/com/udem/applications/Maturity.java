package com.udem.applications;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.opencsv.exceptions.CsvException;
import com.udem.entities.Csv;
import com.udem.entities.Metric;
import com.udem.entities.ProgramFile;
import com.udem.services.CsvUtils;

// Evaluate the maturity of a class in terms of the nhc and age metric
public class Maturity implements CsvUtils {
    // Static Data
    private static final String[] header = { "chemin du fichier", "nom du paquet",
            "nom de la classe", "nch metric", "age metric", "cm metric" };
    private static final String configPath = "./maturity.properties";

    public static void main(String args[]) throws IOException, InterruptedException, CsvException {
        Properties prop = loadConfig(configPath);
        Jls.main(args);
        List<String[]> jlsFiles = CsvUtils.read("jls_output.csv");
        Csv jlsCsv = new Csv();
        jlsCsv.parse(jlsFiles, "jls");
        Csv maturityCsv = new Csv();
        String inputPath = evaluateInputPath(args[0]);
        evaluate(jlsCsv.files, prop.getProperty("TOKEN"), maturityCsv, inputPath);
        ArrayList<String[]> files = new Maturity().produceOuputContent(maturityCsv.files);
        CsvUtils.write(files, "cm_output.csv", header);
        System.out.println("Cm is done!");
    }

    private static Properties loadConfig(String configPath) throws IOException {
        FileInputStream propsInput = new FileInputStream(configPath);
        Properties prop = new Properties();
        prop.load(propsInput);
        return prop;
    }

    private static String evaluateInputPath(String inputPath){
        if (inputPath.substring(inputPath.length() - 1) == "/") {
            return inputPath.substring(1);
        } else {
            return inputPath;
        }
    }

    private static String evaluateGitAbsoluteClassPath(String inputPath, String relativeClassPath) {
        // Jls relative class path contains parent directories of the src application
        // folder
        if (relativeClassPath.contains("/src/")) {
            return "/src/" + relativeClassPath.split("/src/")[1];
            // Jls relative class path is missing the src parent directory
        } else {
            String[] directories = inputPath.split("src");
            if (directories.length == 1) {
                return "/src" + relativeClassPath.substring(1);
            } else {
                return "/src" + inputPath.split("/src")[1] + relativeClassPath.substring(1);
            }
        }

    }

    private static List<Integer> getCommits(String path, String apiToken) throws IOException, InterruptedException {

        List<Integer> list = new ArrayList<Integer>();

        // create client
        HttpClient client = HttpClient.newHttpClient();

        // create request
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://api.github.com/repos/jfree/jfreechart/commits?path="
                        + path + "&per_page=100"))
                .header("Authorization", "token " + apiToken)
                .build();

        HttpResponse<String> response = client.send(request,
                HttpResponse.BodyHandlers.ofString());

        if (response.statusCode() != 200) {
            System.out.println("Rate limit error for NCH");
        }
        if (!response.body().equals("[]")) {
            Gson g = new Gson();
            ArrayList res = g.fromJson(response.body(), ArrayList.class);
            System.out.println("");
            String regexDate = "date=(.*?)[\\s]";
            Pattern p = Pattern.compile(regexDate);
            Matcher m = p.matcher(res.get(res.size() - 1).toString());
            boolean did = m.find();
            int creationYearsAgo = 0;
            if (did) {
                creationYearsAgo = 2022 - Integer.parseInt(m.group(1).substring(0, 4));
            }
            list.add(0, res.size());
            list.add(1, creationYearsAgo);
            System.out.println("Done " + path);
            return list;
        } else {
            System.out.println("There was an error contacting GitHub API for NCH");
        }
        return list;
    }

    private static void evaluate(ArrayList<ProgramFile> csvLines, String apiToken,
            Csv maturityCsv, String inputPath)
            throws IOException, InterruptedException {

        System.out.println("Running maturity metric pipeline");
        for (ProgramFile line : csvLines) {
            String gitAbsoluteClassPath = evaluateGitAbsoluteClassPath(inputPath, line.getPathToFile());
            List<Integer> results = getCommits(gitAbsoluteClassPath, apiToken);
            Integer nchMetric = results.get(0);
            Integer ageMetric = results.get(1);
            Integer cmMetric = nchMetric * ageMetric;
            maturityCsv.addProgramFile(
                    new ProgramFile(line.getPathToFile(),
                            line.getPaquetName(), line.getClassName(),
                            new ArrayList<>(Arrays.asList(
                                    new Metric(Integer.toString(nchMetric), "nch"),
                                    new Metric(Integer.toString(ageMetric), "age"),
                                    new Metric(Integer.toString(cmMetric), "cm")))));
        }
    }

    // CM metric value assessment is base on the business personnal scale
    // shown in the README.md file
    public static int[] calculateMaturityClasses(ArrayList<ProgramFile> cmFiles) {
        int[] maturityCount = new int[5];

        for (ProgramFile file : cmFiles) {
            int cmMetric = Integer.parseInt(file.getMetrics().get(2).getValue());
            if (cmMetric <= 2) {
                maturityCount[0] += 1;
            } else if (cmMetric <= 10) {
                maturityCount[1] += 1;
            } else if (cmMetric <= 30) {
                maturityCount[2] += 1;
            } else if (cmMetric <= 40) {
                maturityCount[3] += 1;
            } else {
                maturityCount[4] += 1;
            }
        }
        return maturityCount;
    }

    @Override
    public ArrayList<String[]> produceOuputContent(ArrayList<ProgramFile> file) {
        ArrayList<String[]> files = new ArrayList<>();
        for (ProgramFile line : file) {
            ArrayList<String> data = new ArrayList<>(Arrays.asList(line.getPathToFile(),
                    line.getPaquetName(), line.getClassName()));
            for (Metric metric : line.getMetrics()) {
                data.add(metric.getValue());
            }
            files.add(data.toArray(new String[0]));
        }
        return files;
    }
}
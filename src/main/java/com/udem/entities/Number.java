package com.udem.entities;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Number {
    
    public static double round(double number){
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        return Double.parseDouble(df.format(number));
    }
}

package com.udem.entities;

import java.util.ArrayList;
import java.util.List;

public class Csv {
    public ArrayList<ProgramFile> files = new ArrayList<>();

    public Csv(ArrayList<ProgramFile> files) {
        this.files = files;
    }

    public Csv() {
    }

    public void addProgramFile(ProgramFile file) {
        files.add(file);
    }

    public void parse(List<String[]> inputFiles, String metricName) {
        for (String[] file : inputFiles) {
            String pathToFile = null;
            String paquetName = null;
            String className = null;
            ArrayList<Metric> metrics = new ArrayList<>();

            try {
                pathToFile = file[0];
            } catch (Exception e) {
            }
            try {
                paquetName = file[1];
            } catch (Exception e) {
            }
            try {
                className = file[2];
            } catch (Exception e) {
            }

            for (int i = 3; i < file.length; i++) {
                try {
                    metrics.add(new Metric(file[i], null));
                } catch (Exception e) {
                }
            }
            addProgramFile(new ProgramFile(pathToFile, paquetName, className, metrics));
        }
    }
}

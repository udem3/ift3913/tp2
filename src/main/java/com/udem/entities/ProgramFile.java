package com.udem.entities;

import java.util.ArrayList;

public class ProgramFile {
    private String pathToFile;
    private String paquetName;
    private String className;
    private String relativePath;
    public ArrayList<Metric> metrics;

    public ProgramFile(String pathToFile, String paquetName,
     String className, ArrayList<Metric> metrics) {
        this.pathToFile = pathToFile;
        this.paquetName = paquetName;
        this.className = className;
        this.metrics = metrics;
    }

    public String getPathToFile() {
        return this.pathToFile;
    }

    public void setPathToFile(String pathToFile) {
        this.pathToFile = pathToFile;
    }

    public String getPaquetName() {
        return this.paquetName;
    }

    public void setPaquetName(String paquetName) {
        this.paquetName = paquetName;
    }

    public String getClassName() {
        return this.className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getRelativePath() {
        return this.relativePath;
    }

    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }

    public ArrayList<Metric> getMetrics() {
        return this.metrics;
    }

    public void setMetrics(ArrayList<Metric> metrics) {
        this.metrics = metrics;
    }    

}

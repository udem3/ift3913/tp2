package com.udem.entities;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import com.opencsv.exceptions.CsvException;
import com.udem.services.CsvUtils;

public class Metric {
    private String value;
    private String name;

    public Metric(String value, String name) {
        this.value = value;
        this.name = name;
    }

    public static Csv parse(String metricFileName, String name) throws FileNotFoundException,
     IOException, CsvException {
        Csv metricCsv = new Csv();
        List<String[]> metricFiles = CsvUtils.read(metricFileName);
        metricCsv.parse(metricFiles, name);
        return metricCsv;
    }

    public static double calculateThreshold(int qualifiedClass, int numberClass) {
        return (double) qualifiedClass / numberClass;
    }

    public static boolean evaluateThreshold(double threshold) {
        if (threshold >= 0.90) {
            return true;
        } else {
            return false;
        }
    }


    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
   
}

#!/bin/sh

ll="20162457"
ah="20181634"

rm -r release

if [ ! -d "release" ] 
then
    mkdir release
    mkdir release/tp2_${ll}_${ah}
    mkdir release/tp2_${ll}_${ah}
    mkdir release/tp2_${ll}_${ah}/csv
    touch release/tp2_${ll}_${ah}/csv/.gitignore
else
    files=$(shopt -s nullglob dotglob; echo release/tp2_${ll}_${ah}/.jar)
    if (( ${#files} ))
    then
        rm release/tp2_${ll}_${ah}/*.jar
    fi
fi

mvn package

cp README.md release/tp2_${ll}_${ah}/README.txt
cp ./csv/*.csv release/tp2_${ll}_${ah}/csv
cp ./*.properties release/tp2_${ll}_${ah}/
cp ./IFT3913_TP2_20162457_20181634.pdf release/tp2_${ll}_${ah}/

cd release && zip -r "tp2_${ll}_${ah}" tp2_${ll}_${ah}

echo "Files can be found in the release directory"